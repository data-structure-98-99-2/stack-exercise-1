class Stack {
  constructor(maxSize = 10) {
    this.MAX_SIZE = maxSize;
    this.array = [0];
  }

  isFull() {
    return this.array[0] === this.MAX_SIZE;
  }

  isEmpty() {
    return this.array[0] === 0;
  }

  push(item) {
    if (this.isFull()) throw new Error("Stack is full");
    this.array[0]++;
    this.array.push(item);
  }

  pop() {
    if (this.isEmpty()) throw new Error("Stack is empty");
    this.array[0]--;
    return this.array[this.array[0] + 1];
  }
}

module.exports = Stack;

# Stack Exercise 1

## Directory Structure

The source of project is stored under `src` directory

The test files are stored under `tests` directory

## Running Tests

For running tests `node.js` should be installed on your machine

First install dev dependencies after change working directory to project directory:

```
npm i
```

Then you can run tests by running this command:

```
npm run test
```

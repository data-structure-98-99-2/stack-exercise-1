const Stack = require("../../src/stack");

test("check push and pop functions", () => {
  const stack = new Stack();
  stack.push(2);
  expect(stack.pop()).toBe(2);
});

test("check push function", () => {
  const stack = new Stack(2);
  stack.push(1);
  stack.push(2);
  expect(() => stack.push(3)).toThrow("Stack is full");
});

test("check pop function", () => {
  const stack = new Stack(2);
  stack.push(1);
  stack.pop();
  expect(() => stack.pop()).toThrow("Stack is empty");
});
